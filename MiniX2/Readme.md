## MiniX2 - Geometric emoji

![](Geometric_emojis.png)

**Describe your program/what you have learnt**
For this MiniX I have made two abstract emojis. I wanted to make something that didn't looked like the 'normal' emojis, we know from our phones, tablets and computers. The emojis have evolved so much from my childhood to now. At the beginning you could only make emojis as the little yellow face which could blink, smile, smile with teeth and so on. These kind of emojis was made with help from the different signs as . , : > and ( ). As the emojis evolved more it almost felt like you where using some kind of cheat-codes if you could make a penguin or at least something different from the round, little and yellow face. Now a days we have a whole keyboard with more emojis that we use. Why do we need nine different face expressions on a cat or monkey?

Therefore I thought it would be interesting to challenge peoples thoughts about how emojis should look like. Therefore are my emojis not perfectly round, have different sizes and are not filled with a color. The emojis are made out of different shapes, arcs, curves and lines. To make the heads of the emojis, I used the syntax beginShape( ); with two different endings such as endShape( ); and endShape(CLOSE);. The differences between these two is that endShape(CLOSE); closes the shape , while endShape(); leaves a gap between the first x and y coordinates in the shape and the last coordinates of the shape.
One of the things/shapes I had a lot of difficulties to use was the curves. I didn't quite understood the controllingpoint and their meaning for the curve. But after a lot of try and error I figured out how to use the syntax.

Otherwise than challegen or even provoke peoples thoughts about emojis and the visualization of them, I wanted to make some kind of emoji that looked more like art than an emoji.

**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?**
Reading "Modifying the Universal" about how people feel they are not
represented in the emojis we have nowadays made me a bit divided. I do not think that emojis should be used to represent humans. I think they are a fun little part of writing, where it is possible for us to express our reaction to a text, video or picture.
I find it a bit overwhelming that humans wants to identify themselfs with emojis. I know there is different skintones, genders and other things which are underrepresented in the world of emojis, but it is a world of something that is made for us to have fun, when we are texting with each other.
Therefor I wanted to make emojis that just represented your mood/emotions. I did not want people to feel excluded and that is why the emojis does not look like humans. They should not represent a skintone, gender or occupation.I wanted them to represent everyone no matter what race, gender, skintone or occupation you feel you belong to.

## Links to Project
- [Execute my project](https://maikenchristiansen.gitlab.io/aestetisk-programmering/MiniX2/)
- [Link to code](https://gitlab.com/MaikenChristiansen/aestetisk-programmering/-/blob/main/MiniX2/sketch.js)

### References
- Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51.
- [The text](https://p5js.org/reference/#/p5/text)
- [The rotation of the arcs](https://p5js.org/reference/#/p5/rotate)
- [The different shapes](https://p5js.org/reference/#/p5/beginShape)
- [Push and pop](https://www.youtube.com/watch?v=o9sgjuh-CBM)

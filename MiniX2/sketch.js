//Variablen for angle
let angle =0;

function setup() {
  createCanvas(windowWidth,windowHeight);

}
function draw() {
//The color of the background
    background(0,0,0);
    
//Question
    let w = "What is the purpose with emojis?"
    strokeWeight(0);
    stroke(255);
    textSize(20);
    fill(255);
    text(w,60,390,500,300);
    noFill();

//The color of the stroke and size
    strokeWeight(1);
    stroke(255,255,255);

//The shape of the first emojis head
    beginShape();
    vertex(150,54);
    vertex(53,155);
    vertex(87,295);
    vertex(123,327);
    vertex(231,348);
    vertex(323,287);
    vertex(331,117);
    vertex(293,77);
    endShape(CLOSE);

//The nose of the first emoji
    noFill();
    beginShape();
    vertex(182, 156);
    vertex(157, 252);
    vertex(215, 252);
    endShape();

//The right eye of the first emoji
    noFill();
    strokeJoin(BEVEL);
    beginShape();
    vertex(216, 156);
    vertex(250, 181);
    vertex(298, 156);
    endShape();

//The left eye of the first emoji
    arc(120, 165, 70, 70, 80, PI + QUARTER_PI);

//The mouth of the fist emoji
    noFill();
    beginShape();
    vertex(120, 270);
    vertex(270, 270);
    vertex(215, 305);
    vertex(150, 295);
    endShape(CLOSE);

//The arc of the first emoji head
push()
//The point that the arc rotate around
    translate(195,205);
    rotate(angle);

    strokeWeight(4);
    stroke(0,255,255)
//Because we have declared the point the arc should rotate around
//The X & Y coordinates for the arc is 0
    arc(0, 0, 310, 300, 6, PI + QUARTER_PI);
    describe('white ellipse with top right quarter missing');
    noFill();
pop()

//The color of the stroke
    strokeWeight(1);
    stroke(255,255,255);

//The shape of the second emoji head
    beginShape();
    vertex(210,451);
    vertex(297,484);
    vertex(350,586);
    vertex(304,710);
    vertex(160,746);
    vertex(77,684);
    vertex(62,543);
    vertex(144,464);
    endShape(CLOSE);

//The nose and eyebrow of the second smiley
    noFill();
    beginShape();
    vertex(100,526);
    vertex(190,526);
    vertex(190,625);
    vertex(220,625);
    endShape();

//The left eye of the second emoji
    arc(135, 550, 50, 70, 5, PI + QUARTER_PI, OPEN);

//The right eye of the second emoji
    curve(204, 450, 215, 547, 294, 547, 294, 470);

//The eyelashes of the second emoji
    line(222,553,215,570);
    line(236,556,230,575);
    line(250,559,247,577);
    line(263,559,267,577);
    line(277,556,285,572);

//The mouth of the second emoji
    line(265, 670, 130, 670);

//The arc of the second emoji
push()
//The point that the arc rotate around
    translate(190,590);
    rotate(angle);

    strokeWeight(4);
    stroke(204,0,0);
//Because we have declared the point the arc should rotate around
//The X & Y coordinates for the arc is 0
    arc(0, 0, 300, 300, PI + QUARTER_PI, TWO_PI);
    describe('shattered outline of ellipse with a quarter of a white circle bottom-right');
pop()

angle=angle+0.01;
}

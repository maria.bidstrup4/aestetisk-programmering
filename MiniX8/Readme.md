## Voices of Climate

Our program can be seen on Josefine Stenshoj GitLab

- [Our program](https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX8/)
- [Our code](https://gitlab.com/josefinestenshoj/aestheticprogramming/-/blob/main/MiniX8/sketch.js)


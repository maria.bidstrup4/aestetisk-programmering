let jerrySize = {
  w:110,
  h:100
};
let jerry;
let jerryY;
let mini_height;
let min_cheese = 6;  //min cheese on the screen
let cheese = [];
let score =0, lose = 0;
let keyColor = 45;

function preload(){
  jerry = loadImage("mouse.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  jerryY = height/2;
  mini_height = height/2;
}
function draw() {
  background("skyblue");
  fill(keyColor, 255);
  rect(0, height/1.5, width, 1);
  displayScore();
  checkCheeseNum(); //available cheese
  showCheese();
  image(jerry, 0, jerryY, jerrySize.w, jerrySize.h);
  checkEating(); //scoring
  checkResult();
}

function checkCheeseNum() {
  if (cheese.length < min_cheese) {
    cheese.push(new Cheese());
  }
}

function showCheese(){
  for (let i = 0; i <cheese.length; i++) {
    cheese[i].move();
    cheese[i].show();
  }
}

function checkEating() {
  //calculate the distance between each cheese
  for (let i = 0; i < cheese.length; i++) {
    let d = int(
      dist(jerrySize.w/2, jerryY+jerrySize.h/2,
        cheese[i].pos.x, cheese[i].pos.y)
      );
    if (d < jerrySize.w/2.5) { //close enough as if collecting the cheese
      score++;
      cheese.splice(i,1);
    }else if (cheese[i].pos.x < 3) { //Jerry lost the cheese
      lose++;
      cheese.splice(i,1);
    }
  }
}

// Creating the class
class Cheese{
// Properties of the obejct
constructor(){
this.speed=floor(random(2,5));
this.pos = new createVector(width+5, random(12, height/1.7));
this.size = floor(random(15, 35));

}
move(){
this.pos.x-=this.speed;
}
show(){
fill("yellow")
triangle(this.pos.x+40,this.pos.y-20,this.pos.x+40,this.pos.y+20,this.pos.x-20,this.pos.y)
}
}


function displayScore() {
    fill(keyColor, 160);
    textSize(17);
    text('You have collected '+ score + " pieces of cheese", 10, height/1.4);
    text('You have lost ' + lose + " pieces of cheese", 10, height/1.4+20);
    fill(keyColor,255);
    text('PRESS the ARROW UP & DOWN key to collect cheese',
    10, height/1.4+40);
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(keyColor, 255);
    textSize(26);
    textAlign(CENTER);
    text("GAME OVER...", width/2, height/2);
    textAlign(CENTER);
    text("You have lost too many pieces of cheese", width/2+30,height/2+30);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    jerryY-=50;
  } else if (keyCode === DOWN_ARROW) {
    jerryY+=50;
  }
  //reset if the Jerry moves out of range
  if (jerryY > mini_height) {
    jerryY = mini_height;
  } else if (jerryY < 0 - jerrySize.w/2) {
    jerryY = 0;
  }

  // Prevent default browser behaviour
  // attached to key events.
  return false;
}

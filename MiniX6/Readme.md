### Jerry collecting cheese

![](JerryPlay.gif)

- [My program](https://maikenchristiansen.gitlab.io/aestetisk-programmering/MiniX6/)
- [My code](https://gitlab.com/MaikenChristiansen/aestetisk-programmering/-/blob/main/MiniX6/sketch.js)

The topic for this MiniX is Object-Oriented Programming - "Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic"
We have worked with what objects are and how they can be used in programming. Objects can be everything - humans, furniture, and so on, the one thing with objects is that they are something we can describe. If I was an object, I would probably be described with words like name/Maiken, brown hair, blue eyes, and 172 cm high all these things there are described are the properties of the object(me).

When you want to use object-oriented programming there are 5 different steps
1.	Naming
2.	Properties
3.	Behaviors
4.	Object creation and usage
5.	Trigger point and logic

It is important to mention that these steps don’t necessarily have to be listed in a program exactly as above. The important part of it is that these steps plays a huge role when talking about object-oriented programming.

I have created a very similar game to the one we tried in class with Pacman eating tofus. There are different reasons for that, but the main reason is that I have been sick with Corona and found the topic of this week very difficult.
The inspiration for my program came from my childhood, I simply loved to watch cartoons (who doesn’t?), and I watched a lot of Tom & Jerry. I loved the games there were with them as characters because I felt like I knew them. I always felt sorry for Jerry because Tom was always chasing him, so I thought it was very cool when I could mock Tom as the character of Jerry in games.
My program is about Jerry collecting cheese, it is possible to move him up and down because of the use of the KeyPressed() syntax with the arrow keys up and down. When you tap the key up Jerry will move 50 up on the Y-axis, when pressing the down arrow, he will move 50 down the Y-axis.  

To use object-oriented programming, I have created a class called Cheese and given it some properties and behaviors.
The triangles/cheese I use in my program can also be described by properties such as they are filled with yellow and has a black stroke, and so on. Besides the properties, the triangles also have something called behavior. The behavior tells us what the object is doing, in this case, they are moving from left to right and there are always 5 visible on the canvas.  To summarize you can say that the properties of an object are about how it looks like, and the behaviors are about what the object is doing, “all these hold and manage data so it can be used and operations can be performed”.

To implement different rules in my program I have some trigger points. One of the trigger points I have created is to make sure whether a piece of cheese is collected or lost. Because the cheese is created as an object it will stay in the program even when it isn’t visible on the screen anymore until it is deleted. In this case, there are 2 options for deleting the cheese

1.	The cheese object is not collected and moves beyond the edges of the program
2.	The cheese object is collected

I use the syntax cheese.length to check the minimum number of cheese objects on the screen. To make the program delete the cheese whether it is collected or lost I use the cheese.splice syntax. This syntax is deleting the cheese object if the distance between the cheese and less than half of the width of Jerrys image the cheese is collected. But the program is also deleting the cheese if the X-position of the cheese object is less than the value of three.



### References
[Aesthetic programming - W. Soon & G. Cox](https://aesthetic-programming.net/pages/4-data-capture.html#minix-capture-all-15243)

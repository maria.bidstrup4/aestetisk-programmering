
let bogstaver = [];//Create an open variable to all the letters in the alphabet
let screen = 0; //Begin on introductionScreen


function setup (){ //Creating Canvas
createCanvas(windowWidth, windowHeight);;
}

function draw(){
  if (screen==0){ //If the screen is equal to 0 show the introductionScreen
    introductionScreen();
  }else if (screen==1){ //If the screen is equal to 1 show the drawingScreen
    drawingScreen();
  }else if (screen==2){ //If the screen is equal to 2 show screen 2
  }else if(screen==3){ //If the screen is equal to 3 show screen 3
  }

}

function introductionScreen (){ //Designing the instroductionScreen
  background('pink');
  textAlign(CENTER);
  stroke('black');
  textSize(40);
  text('Want to create your own unique art?', width/2,height/4);
  text('Try to type your name in lower case letters and press enter',width/2,height/2.5);
  text('After pressing enter try to open your console', width/2,height/2.2)
  text('Press the mouse to start', width/2,height/1.2)
}

function drawingScreen(){ //Designing the drawingScreen
  background('pink');

}

function mousePressed(){ //If the mouse is pressed when showing screen 0, switch to startDrawing
if(screen==0){
  startDrawing();
  }
}


function startDrawing(){ //startDrawing = screen 1/drawingScreen
  screen=1;
}


function keyTyped(){ //when keyTyped show screen 2, with the details below
  screen=2;
  shapeColor=color(random(255),random(255),random(255));//random color pallette
  strokeWeight(random(2,10)); //Random strokeWeight between 2-10
  noFill(); //No shapes should have fill

push();
    if (key === 'a') { //If key a i typed draw a line with random variables inside the width and height
      bogstaver.push('a') // Push 'a' into the array
      stroke(shapeColor); // The color there is defined above
      line(random(width),random(height), random(width), random(height));}

//The process for all the letters is more or less the same.

    else if(key === 'b') {
      bogstaver.push('b')
      stroke(shapeColor);
      ellipse(random(width),random(height), random(150), random(150));}

    else if(key === 'c'){
      bogstaver.push('c')
      stroke(shapeColor);
      rect(random(width),random(height), random(20), random(250));}

    else if(key === 'd'){
      bogstaver.push('d')
      stroke(shapeColor);
      triangle(random(width),random(height), random(height), random(width),random(width),random(height));}

    else if(key === 'e'){
      bogstaver.push('e')
      noFill();
      stroke(shapeColor);
      strokeWeight(random(10));
      arc(random(width),random(height), 80, 80, random(0),PI + QUARTER_PI, OPEN);}

    else if(key === 'f'){
      bogstaver.push('f')
      stroke(shapeColor)
      point(random(width),random(height));}

    else if(key === 'g'){
      bogstaver.push('g')
      stroke(shapeColor)
      quad(random(width),random(height),random(width),random(height),random(width),random(height),random(width),random(height));}

    else if(key === 'h'){
      bogstaver.push('h')
      stroke(shapeColor)
      rect(random(width),random(height),random(width),random(height), 20);}

    else if(key === 'i'){
      bogstaver.push('i')
      stroke(shapeColor)
      strokeJoin(MITER);
      beginShape();
      vertex(random(width),random(height));
      vertex(random(width),random(height));
      vertex(random(width),random(height));
      endShape();}

    else if(key === 'j'){
      bogstaver.push('j')
      stroke(shapeColor)
      bezier(random(width),random(height),random(width),random(height),random(width),random(height),random(width),random(height));}
  pop();

  push();
    if (key === 'k') {
      bogstaver.push('k')
      stroke(shapeColor);
      line(random(width),random(height), random(width), random(height));}

    else if(key === 'l') {
      bogstaver.push('l')
      stroke(shapeColor);
      ellipse(random(width),random(height), random(150), random(150));}

    else if(key === 'm'){
      bogstaver.push('m')
      stroke(shapeColor);
      rect(random(width),random(height), random(20), random(250));}

    else if(key === 'n'){
      bogstaver.push('n')
      stroke(shapeColor);
      triangle(random(width),random(height), random(height), random(width),random(width),random(height));}

    else if(key === 'o'){
      bogstaver.push('o')
      noFill();
      stroke(shapeColor);
      strokeWeight(random(10));
      arc(random(width),random(height), 80, 80, random(0),PI + QUARTER_PI, OPEN);}

    else if(key === 'p'){
      bogstaver.push('p')
      stroke(shapeColor)
      point(random(width),random(height));}

    else if(key === 'q'){
      bogstaver.push('q')
      stroke(shapeColor)
      quad(random(width),random(height),random(width),random(height),random(width),random(height),random(width),random(height));}

    else if(key === 'r'){
      bogstaver.push('r')
      stroke(shapeColor)
      rect(random(width),random(height),random(width),random(height), 20);}

    else if(key === 's'){
      bogstaver.push('s')
      stroke(shapeColor)
      strokeJoin(MITER);
      beginShape();
      vertex(random(width),random(height));
      vertex(random(width),random(height));
      vertex(random(width),random(height));
      endShape();}

    else if(key === 't'){
      bogstaver.push('t')
      stroke(shapeColor)
      bezier(random(width),random(height),random(width),random(height),random(width),random(height),random(width),random(height));}
  pop();

  push();
    if (key === 'u') {
      bogstaver.push('u')
      stroke(shapeColor);
      line(random(width),random(height), random(width), random(height));}

    else if(key === 'v') {
      bogstaver.push('v')
      stroke(shapeColor);
      ellipse(random(width),random(height), random(150), random(150));}

    else if(key === 'w'){
      bogstaver.push('w')
      stroke(shapeColor);
      rect(random(width),random(height), random(20), random(250));}

    else if(key === 'x'){
      bogstaver.push('x')
      stroke(shapeColor);
      triangle(random(width),random(height), random(height), random(width),random(width),random(height));}

    else if(key === 'y'){
      bogstaver.push('y')
      noFill();
      stroke(shapeColor);
      strokeWeight(random(10));
      arc(random(width),random(height), 80, 80, random(0),PI + QUARTER_PI, OPEN);}

    else if(key === 'z'){
      bogstaver.push('z')
      stroke(shapeColor)
      point(random(width),random(height));}

    else if(key === 'æ'){
      bogstaver.push('æ')
      stroke(shapeColor)
      quad(random(width),random(height),random(width),random(height),random(width),random(height),random(width),random(height));}

    else if(key === 'ø'){
      bogstaver.push('ø')
      stroke(shapeColor)
      rect(random(width),random(height),random(width),random(height), 20);}

    else if(key === 'å'){
      bogstaver.push('å')
      stroke(shapeColor)
      strokeJoin(MITER);
      beginShape();
      vertex(random(width),random(height));
      vertex(random(width),random(height));
      vertex(random(width),random(height));
      endShape();}

    else if (key===' ') //Here I want the to make space if the spacebar is pressed
    bogstaver.push(' ') // The spacebare is not connected with a shape

  pop();
}

function keyPressed(){ //When enter is pressed show screen 3
  screen=3;
  if(keyCode ==13 ); {
  for(let i=0;i<bogstaver;i++){
  }}
  console.clear(...bogstaver)//Clear the console, show it doesn't show each line, but only the line after enter is pressed
  console.log("Your name is", ...bogstaver); //Show the console with the array as a string - The three dots.

}
